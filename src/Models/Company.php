<?php

namespace Inmovsoftware\CompanyApi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;
    protected $table = "it_business";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $dates = ['deleted_at'];
    protected $fillable = ['name', 'alias', 'logo', 'address', 'it_cities_id', 'phone', 'languaje', 'latitude', 'longitude', 'placeid', 'facebook', 'instagram', 'twitter', 'youtube', 'linkedin'];


}
