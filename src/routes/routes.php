<?php
use Illuminate\Http\Request;

Route::middleware(['api', 'jwt', 'cors'])->group(function () {
Route::group([
    'prefix' => 'admin'
], function () {
    Route::apiResource('company', 'Inmovsoftware\CompanyApi\Http\Controllers\CompanyController');
        });
});
